void vypisVysledkuTestu(bool result, string menoTestu){
   cout << "------------> " << menoTestu << ":  - ";
   cout << (result ? "OK" : "FALSE") << endl << endl;
}
bool testyDatumu(){
  bool result = true;

  vypisVysledkuTestu(result, "Zakladne testy");
  return result;
}
bool testyKontroly(){
  bool result = true;

  vypisVysledkuTestu(result, "Testy kontroly");
  return result;
}

bool testyDruhehoFormatu(){
  bool result = true;
  DATUM datum;
  result &= datum.vlozDatum("10/31/3000");
  result &= datum.vlozDatum("4/31/1999");

  vypisVysledkuTestu(result, "Testy druheho formatu");
  return result;
}



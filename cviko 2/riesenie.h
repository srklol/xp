bool DUMMY_BOOL = false;
string DUMMY_STRING = "";

class DATUM {
  int den, mesiac, rok;
 public:
  DATUM();
  bool vlozDatum(const string &datum);
  string vratDatum();
};
DATUM::DATUM():den(1), mesiac(1), rok(1970)
{
}

bool DATUM::vlozDatum(const string &datum) {
	int dniVMesiaci[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	char delimiter;
	bool delimSet = false;
	if (datum == "") return false;
	string s = "";
	int counter = 0;
	int tempDen = 0, tempMesiac = 0, tempRok = 0;
	for (unsigned int i = 0; i < datum.length(); i++){
		if (datum[i] >= 48 && datum[i] <= 57){
			s += datum[i];
		}
		else {
			if (datum[i] == '.' || datum[i] == '/'){
				if (!delimSet){
					delimiter = datum[i];
					delimSet = true;
				}
				else {
					if(datum[i] == delimiter){
					}
					else {
						return false;
					}
				}
				stringstream ss(s);
				switch (counter){
				case 0:
					if (delimiter == '.'){
						ss >> tempDen;
					}
					else {
						ss >> tempMesiac;
					}
					break;
				case 1:
					if (delimiter == '.'){
						ss >> tempMesiac;
					}
					else {
						ss >> tempDen;
					}
					break;
				}
				s = "";
				counter++;
			}
			else if (datum[i] == '-'){
				if (i < (datum.size() - 2)){
					if (datum[i + 1] == '.'){
						return false;
					}
				}
				s += '-';
			}
			else {
				return false;
			}
		}
	}

	if (datum[datum.size() - 1] < 48 || datum[datum.size() - 1] > 57) return false;
	stringstream ss(s);
	ss >> tempRok;

	if (tempDen <= 0) return false;
	if (tempMesiac <= 0) return false;

	if (tempMesiac > 12) return false;
	if (tempRok % 4 == 0) {
		dniVMesiaci[1] = 29;
	}
	if (tempDen > dniVMesiaci[tempMesiac - 1]) return false;
/*
	if (tempRok < 0) return false;
	if (tempRok % 4 == 0 && tempMesiac == 2 && tempDen <= 29) return false;
*/
	den = tempDen;
	mesiac = tempMesiac;
	rok = tempRok;
	return true;
}
string DATUM::vratDatum() {
  stringstream ss;
  ss << den << "." << mesiac << "." << rok;
  return ss.str();
}


bool testTrue(bool t1, string nazovTestu){
  if (t1){
      cout << nazovTestu << ": " << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu  << ": " << " - FALSE" << endl;
      return false;
  }
}

bool testFalse(bool t1, string nazovTestu){
  if (!t1){
      cout << nazovTestu  << ": " << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu  << ": " << " - FALSE" << endl;
      return false;
  }
}

bool testEqual(int i1, int i2, string nazovTestu){
  if (i1 == i2){
      cout << nazovTestu  << ": " << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu  << ": " << " - FALSE" << endl;
    return false;
  }
}
bool testNonEqual(int i1, int i2, string nazovTestu){
  if (i1 != i2){
      cout << nazovTestu  << ": " << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu  << ": " << " - FALSE" << endl;
    return false;
  }
}
bool testEqual(double d1, double d2, string nazovTestu){
  if (d1 == d2){
      cout << nazovTestu  << ": " << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu  << ": " << " - FALSE" << endl;
      return false;
  }
}
bool testNonEqual(double d1, double d2, string nazovTestu){
  if (d1 != d2){
      cout << nazovTestu  << ": " << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu  << ": " << " - FALSE" << endl;
      return false;
  }
}
bool testEqual(string s1, string s2, string nazovTestu){
  if (s1 == s2){
      cout << nazovTestu  << ": " << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu  << ": " << " - FALSE" << endl;
    return false;
  }
}

bool testNonEqual(string s1, string s2, string nazovTestu){
  if (s1 != s2){
      cout << nazovTestu  << ": " << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu  << ": " << " - FALSE" << endl;
    return false;
  }
}

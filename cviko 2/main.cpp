#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;

#include "eval.h"
#include "riesenie.h"
#include "testy.h"

int main () {
  bool result = true;

  result &= testyDatumu();
  result &= testyKontroly();
  result &= testyDruhehoFormatu();

  if(result) cout << endl << "All tests - OK" << endl ;
  else       cout << endl << "All tests - FAILED" << endl ;
  /*
#ifdef _WIN32
  getchar();
#endif
*/
}


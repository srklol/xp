void vypisVysledkuTestu(bool result, string menoTestu){
   cout << "------------> " << menoTestu << ":  - ";
   cout << (result ? "OK" : "FALSE") << endl << endl;
}

bool testyMorseConverter(){
  bool result = true;

  MORSE_CONVERTER morseConverter;
  /*1.karta - nacitanie kodov znakov zo suboru*/
  morseConverter.loadSymbols("subor1.txt");
  result &= testEqual("abcd efgh ijkl mnop qrst uvw xyz", morseConverter.convertToKey(".- -... -.-. -..   . ..-. --. ....   .. .--- -.- .-..   -- -. --- .--.   --.- .-. ... -   ..- ...- .--   -..- -.-- --.."),"kontrola abecedy po nacitani zo suboru");
  MORSE_CONVERTER morseConverter2;
  morseConverter2.loadSymbols("subor2.txt");
  result &= testEqual("abcd efgh ijkl mnop qrst uvw xyz", morseConverter2.convertToKey(".-|-...|-.-.|-..#.|..-.|--.|....#..|.---|-.-|.-..#--|-.|---|.--.#--.-|.-.|...|-#..-|...-|.--#-..-|-.--|--.."),"kontrola abecedy po nacitani zo suboru");
  /*2.karta - kontrola nepovolenych znakov*/
  result &= testEqual(ZLY_VYRAZ, morseConverter.convertToKey(".- &"), "nepovoleny znak &");
  result &= testEqual(ZLY_VYRAZ, morseConverter.convertToKey(".- -... -.-. $-.."), "nepovoleny znak $");
  result &= testEqual(ZLY_VYRAZ, morseConverter.convertToKey(".- -... -.-. -.. $#@%^&"), "nepovolene znaky $#@%^&");
  result &= testFalse(morseConverter.isValidChar('$'), "nepovoleny znak $");
  result &= testFalse(morseConverter.isValidChar('@'), "nepovoleny znak @");
  result &= testTrue(morseConverter.isValidChar('.'), "validny znak .");
  result &= testTrue(morseConverter.isValidChar('-'), "validny znak -");
  /*3.karta - testy opacneho kovertovania*/
  result &= testEqual(".- -... -.-. -..   . ..-. --. ....   .. .--- -.- .-..   -- -. --- .--.   --.- .-. ... -   ..- ...- .--   -..- -.-- --..", morseConverter.convertToValue("abcd efgh ijkl mnop qrst uvw xyz"),"kontrola opacneho konvertovania");
  result &= testEqual(".-|-...|-.-.|-..#.|..-.|--.|....#..|.---|-.-|.-..#--|-.|---|.--.#--.-|.-.|...|-#..-|...-|.--#-..-|-.--|--..", morseConverter2.convertToValue("abcd efgh ijkl mnop qrst uvw xyz"),"kontrola opacneho konvertovania");
  /*4.karta - kontrola platnych morzeovych znakov*/
  result &= testEqual(ZLY_VYRAZ, morseConverter.convertToKey(".- -... -.-.-.."), "neplatny znak");
  result &= testEqual(ZLY_VYRAZ, morseConverter.convertToKey(".. .----.- .-.."), "neplatny znak");
  result &= testEqual(ZLY_VYRAZ, morseConverter.convertToKey("..-...- .--"), "neplatny znak");
  result &= testEqual(true, morseConverter.charExists(".-"), "existujuci znak");
  result &= testEqual(false, morseConverter.charExists("..-...-"), "existujuci znak");
  vypisVysledkuTestu(result, "Konverter morzeovky");
  return result;
}


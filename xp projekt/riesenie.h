
const bool DUMMY_BOOL = false;
const int DUMMY_INT = 0;
const string DUMMY_STRING = "";
const string SYMBOL_SPACE = "SS";
const string WORD_SPACE = "WS";

const string ZLY_VYRAZ = "Zly vstupny vyraz";

class MORSE_CONVERTER{
	public:
	map<string, string> conversionKeyToValue;
	map<string, string> conversionValueToKey;
	set<char> validChars;
	MORSE_CONVERTER();
	bool loadSymbols(string path);
	string convertToKey(string line);
	string convertToValue(string line);
	bool isValidChar(char character);
	bool isValidLine(string line);
	string truncExcessSpaces(string line);
	bool charExists(string word);
	vector< vector<string> > splitToWords(string line);
	vector<string> splitToSymbols(string word);
	vector<string> splitter(string input, string delimiter);
	string getConversionValue(map<string, string> conversionTable, string key);
	void addCharsToSet(string line);
};

MORSE_CONVERTER::MORSE_CONVERTER(){

}

bool MORSE_CONVERTER::loadSymbols(string path){
	ifstream inFile;
	inFile.open(path.c_str());
	if (!inFile){
		return false;
	}
	string symbol = "", preklad = "";
	while (getline(inFile, symbol, ' ')){
		getline(inFile, preklad);
		preklad.erase(0, 1);
		preklad.erase(preklad.length()-1, 1);
		addCharsToSet(preklad);
		conversionKeyToValue.insert(pair<string, string>(symbol, preklad));
		conversionValueToKey.insert(pair<string, string>(preklad, symbol));
	}
	return true;
}

string MORSE_CONVERTER::convertToKey(string line){
	vector< vector<string> > words = splitToWords(line);
	vector< vector<string> >::iterator i = words.begin();
	vector<string>::iterator ii;
	string result = "";
	while (i != words.end()){
		ii = i->begin();
		while (ii != i->end()){
			if (getConversionValue(conversionValueToKey, *ii) == ZLY_VYRAZ){
				return ZLY_VYRAZ;
			}
			result += getConversionValue(conversionValueToKey, *ii);
			ii++;
		}
		i++;
		if (i != words.end()){
			result += " ";
		}
	}
	return result;
}

string MORSE_CONVERTER::convertToValue(string line){
	string result = "";
	for (unsigned int i = 0; i < line.length(); i++){
		if (line.at(i) == ' '){
			result += getConversionValue(conversionKeyToValue, WORD_SPACE);
		}
		else{
			stringstream ss;
			string s;
			char c = line.at(i);
			ss << c;
			ss >> s;
			result += getConversionValue(conversionKeyToValue, s);
			if (i != line.length() - 1 && line.at(i + 1) != ' '){
				result += getConversionValue(conversionKeyToValue, SYMBOL_SPACE);
			}
		}
	}
	return result;
}

bool MORSE_CONVERTER::isValidChar(char character){
	set<char>::iterator i = validChars.find(character);
	if (i == validChars.end()){
		return false;
	}
	return true;
}

bool MORSE_CONVERTER::charExists(string word){
	return (getConversionValue(conversionValueToKey, word) == ZLY_VYRAZ) ? false : true;
}

vector< vector<string> > MORSE_CONVERTER::splitToWords(string line){
	string wordDelimiter = getConversionValue(conversionKeyToValue, WORD_SPACE);
	vector<string> words;
	vector< vector<string> > result;
	words = splitter(line, wordDelimiter);

	vector<string>::iterator i = words.begin();
	while (i != words.end()){
		result.push_back(splitToSymbols(*i));
		i++;
	}

	return result;
}

vector<string> MORSE_CONVERTER::splitToSymbols(string word){
	string symbolDelimiter = getConversionValue(conversionKeyToValue, SYMBOL_SPACE);
	return splitter(word, symbolDelimiter);
}

vector<string> MORSE_CONVERTER::splitter(string input, string delimiter){
	vector<string> result;
	int delimiterIndex = input.find(delimiter);
	int previousIndex = 0;
	while (delimiterIndex != -1){
		result.push_back(input.substr(previousIndex, delimiterIndex-previousIndex));
		previousIndex = delimiterIndex + delimiter.length();
		delimiterIndex = input.find(delimiter, previousIndex);
	}
	result.push_back(input.substr(previousIndex, input.length()-previousIndex));
	return result;
}

string MORSE_CONVERTER::getConversionValue(map<string, string> conversionTable, string key){
	map<string, string>::iterator i = conversionTable.begin();
	if (conversionTable.count(key) == 0){
		return ZLY_VYRAZ;
	}
	i = conversionTable.find(key);
	if (i != conversionTable.end()){
		return i->second;
	}
	return ZLY_VYRAZ;
}

void MORSE_CONVERTER::addCharsToSet(string line){
	for (unsigned int i = 0; i < line.length(); i++){
		validChars.insert(line.at(i));
	}
}

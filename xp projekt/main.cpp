#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <algorithm>
#include <map>
#include <vector>
#include <set>

using namespace std;

#include "eval.h"
#include "riesenie.h"
#include "testy.h"

int main () {
  bool result = true;

  result &= testyMorseConverter();

  if(result) cout << endl << "All tests - OK" << endl ;
  else       cout << endl << "All tests - FAILED" << endl ;
  /*
#ifdef _WIN32
  getchar();
#endif
   */
}


bool testEqual(string s1, string s2, string nazovTestu){
   if (s1 == s2){
      cout << nazovTestu << ":'" << s1 << "'=='";
      cout << s2 << "' - OK" << endl;
      return true;
   }
   else{
      cout << nazovTestu << ":'" << s1 << "'=='";
      cout << s2 << "' - FALSE" << endl;
      return false;
   }
}
bool testNonEqual(string s1, string s2, string nazovTestu){
   if (s1 != s2){
      cout << nazovTestu << ":'" << s1 << "'!='";
      cout << s2 << "' - OK" << endl;
      return true;
   }
   else{
      cout << nazovTestu << ":'" << s1 << "'!='";
      cout << s2 << "' - FALSE" << endl;
      return false;
   }
}
bool testEqual(int s1, int s2, string nazovTestu){
   if (s1 == s2){
      cout << nazovTestu << ":'" << s1 << "'=='";
      cout << s2 << "' - OK" << endl;
      return true;
   }
   else{
      cout << nazovTestu << ":'" << s1 << "'=='";
      cout << s2 << "' - FALSE" << endl;
      return false;
   }
}
bool testNonEqual(int s1, int s2, string nazovTestu){
   if (s1 != s2){
      cout << nazovTestu << ":'" << s1 << "'!='";
      cout << s2 << "' - OK" << endl;
      return true;
   }
   else{
      cout << nazovTestu << ":'" << s1 << "'!='";
      cout << s2 << "' - FALSE" << endl;
      return false;
   }
}
bool testEqual(double s1, double s2, string nazovTestu){
   if (s1 == s2){
      cout << nazovTestu << ":'" << s1 << "'=='";
      cout << s2 << "' - OK" << endl;
      return true;
   }
   else{
      cout << nazovTestu << ":'" << s1 << "'=='";
      cout << s2 << "' - FALSE" << endl;
      return false;
   }
}
bool testNonEqual(double s1, double s2, string nazovTestu){
   if (s1 != s2){
      cout << nazovTestu << ":'" << s1 << "'!='";
      cout << s2 << "' - OK" << endl;
      return true;
   }
   else{
      cout << nazovTestu << ":'" << s1 << "'!='";
      cout << s2 << "' - FALSE" << endl;
      return false;
   }
}
bool testTrue(bool value, string nazovTestu){
  if (value){
      cout << nazovTestu << " - OK" << endl;
      return true;
  }
  else{
      cout << nazovTestu << " - FALSE" << endl;
      return false;
  }
}
bool testFalse(bool value, string nazovTestu){
  return testTrue(!value, nazovTestu);
}


bool DUMMY_BOOL = 0;
int DUMMY_INT = 0;
int DUMMY_INT_1 = 1;
double DUMMY_DOUBLE = 0.01;
string DUMMY_STRING = "";
char DUMMY_CHAR = '0'; 

class SKUPINA{
   string name;
   map<string, string> zaznamy;
   map<string, string>::iterator aktualnyZaznam;
 public:
   SKUPINA(string n = ""): name(n) { aktualnyZaznam = zaznamy.end();};
   string nazov(){ return name;}
   bool nastavZaznam(string kluc);
   bool pridajZaznam(string kluc,string hodnota);
   bool zmazZaznam(string kluc);
   int pocetZaznamov() { return zaznamy.size();}
   string vratAktualnyKluc();
   string vratAktualnuHodnotu();
   bool vlozAktualnuHodnotu(string hodnota);
   string zrusZleZnakyKluca(string riadok);
   bool jeSpravnyZnakKluca(char znak);
};
bool SKUPINA::nastavZaznam(string kluc) {
  map<string, string>::iterator temp = zaznamy.end();
  if (zaznamy.empty()) {
    return false;
  }
  if (temp == zaznamy.find(kluc)) {
    return false;
  }
  aktualnyZaznam = zaznamy.find(kluc);
  return true;
}
bool SKUPINA::pridajZaznam(string kluc, string hodnota) {
	string tKluc = zrusZleZnakyKluca(kluc);
	if (tKluc == ""){
		return false;
	}
  pair<map<string,string>::iterator,bool> ret;
  bool vysledok=!nastavZaznam(tKluc);
  if (vysledok){
      ret = zaznamy.insert(pair<string, string>(tKluc, hodnota));
      nastavZaznam(tKluc);
  }
  return vysledok;
}
bool SKUPINA::zmazZaznam(string kluc) {
  string aktualnyKluc = vratAktualnyKluc();
  if (!nastavZaznam(kluc)) {
      return false;
  }
  zaznamy.erase(aktualnyZaznam);
  if (aktualnyKluc == kluc){
    aktualnyZaznam = zaznamy.begin();
  }
  else{
    nastavZaznam(aktualnyKluc);
  }
  return true;
}
string SKUPINA::vratAktualnyKluc() {
  if (pocetZaznamov()>0) {
    return aktualnyZaznam->first;
  }
  return "Prazdny";
}
string SKUPINA::vratAktualnuHodnotu() {
  if (pocetZaznamov()>0) {
    return aktualnyZaznam->second;
  }
  return "Prazdny";
}
bool SKUPINA::vlozAktualnuHodnotu(string hodnota) {
  if (pocetZaznamov()>0) {
    aktualnyZaznam->second = hodnota;
    return true;
  }
  return false;
}

string SKUPINA::zrusZleZnakyKluca(string kluc){
	if (kluc == ""){
		return "";
	}
	string res = "";
	bool nacitavanieKluca = true;
	if (kluc.at(0) == '[' && kluc.at(kluc.length() - 1) == ']'){
		return kluc;
	}
	for (unsigned int i = 0; i < kluc.length(); i++){
		if (kluc.at(i) == '='){
			nacitavanieKluca = false;
		}
		if (nacitavanieKluca ){
			if (jeSpravnyZnakKluca(kluc.at(i))){
				res += kluc.at(i);
			}
		}
		else {
			res += kluc.at(i);
		}
	}
	return res;
}

bool SKUPINA::jeSpravnyZnakKluca(char znak){
	if ((znak >= 'a' && znak <= 'z') || (znak >= 'A' && znak <= 'Z') || (znak >= '0' && znak <= '9')){
		return true;
	}
	else
		return false;
}

class SKUPINY{
  map<string, SKUPINA> skupiny;
  map<string, SKUPINA>::iterator actualna;
  SKUPINA *aktualnaSkupina() { return &actualna->second;};
  bool multiLineComment;
public:
  SKUPINY();
  bool nastavSkupinu(string nazov);
  bool pridajSkupinu(string nazov);
  bool zaciatok();
  bool dalsi();
  int pocetSkupin() { return skupiny.size();};
  string nazovAktualnejSkupiny() {return aktualnaSkupina()->nazov();};
//3. uloha
  bool nastavZaznam(string kluc) { return aktualnaSkupina()->nastavZaznam(kluc); };
  bool pridajZaznam(string kluc,string hodnota) { return aktualnaSkupina()->pridajZaznam(kluc, hodnota); };
  bool zmazZaznam(string kluc) { return aktualnaSkupina()->zmazZaznam(kluc); };
  int pocetZaznamov() { return aktualnaSkupina()->pocetZaznamov(); };
  string vratAktualnuHodnotu() {return aktualnaSkupina()->vratAktualnuHodnotu(); };
  bool vlozAktualnuHodnotu(string val) {return aktualnaSkupina()->vlozAktualnuHodnotu(val); };
  bool presunZaznam(string kluc, string doSkupiny);
//4. uloha
  bool spracujRiadok(string pRiadok);
  string vratHodnotu(string kluc);
  bool nacitaj(string cesta);
  string odstranDvojlomitka(string riadok);
  string odstranKomenty(string riadok);
};
SKUPINY::SKUPINY() {
  pridajSkupinu("");
  actualna = skupiny.begin();
  multiLineComment = false;
}
bool SKUPINY::zaciatok() {
  actualna = skupiny.begin();
  //if (DUMMY_BOOL /* ...TODO */) {
  //    return false;
  //}
  return true;
}
bool SKUPINY::dalsi() {
  map<string, SKUPINA>::iterator temp = actualna;
  temp++;
  if (temp == skupiny.end()) {
    return false;
  }
  actualna = temp;
  return true ;
}
bool SKUPINY::nastavSkupinu(string nazov) {
  map<string, SKUPINA>::iterator temp  = skupiny.end();
  if (temp == skupiny.find(nazov)) {
    return false;
  }
  actualna = skupiny.find(nazov);
  return true;
}
//Pokusi sa pridat skupinu s danym nazvom. Ak taka skupina uz existuje, vrati false, inak true.
bool SKUPINY::pridajSkupinu(string nazov) {
  pair<map<string, SKUPINA>::iterator,bool> ret;
  ret = skupiny.insert(pair<string, SKUPINA>(nazov,nazov));;
  if (ret.second) {
    actualna = skupiny.find(nazov);
  }
  return ret.second;
}
bool SKUPINY::presunZaznam(string kluc, string doSkupiny) {
  string povodneAktualnaSkupina=nazovAktualnejSkupiny();
  string povodneAktualnyZaznam=aktualnaSkupina()->vratAktualnyKluc();
  if(!nastavZaznam(kluc)) {
    nastavSkupinu(povodneAktualnaSkupina);
    nastavZaznam(povodneAktualnyZaznam);                            
    return false;
  }
  string povodneAktualnaHodnota=aktualnaSkupina()->vratAktualnuHodnotu();
  if(!nastavSkupinu(doSkupiny)) {
    nastavSkupinu(povodneAktualnaSkupina);
    nastavZaznam(povodneAktualnyZaznam);                    
    return false;
  }
  zmazZaznam(kluc);
  pridajZaznam(kluc,povodneAktualnaHodnota);
  
  nastavSkupinu(povodneAktualnaSkupina);
  zmazZaznam(kluc);
  //if (povodneAktualnyZaznam == kluc) {
  //  nastavZaznam(aktualnaSkupina()->zaznamy.begin())
  //}
  nastavZaznam(povodneAktualnyZaznam);
  
  //bool ret /* = pridajZaznam( ...TODO */;
  /* ...TODO */
  return true;
}
string SKUPINY::vratHodnotu(string kluc) {
  if(!nastavZaznam(kluc)) {
    return "";
  }
  return vratAktualnuHodnotu();
}
bool SKUPINY::spracujRiadok(string pRiadok) {
	string riadok = odstranDvojlomitka(pRiadok);

	if(riadok.length() == 0) {
    return false;
  }
  if(riadok.at(0)=='[') {
    if(riadok.at(riadok.length() -1) != ']' ) {
      return false;
    }
    string groupName = "";
    for(unsigned int i = 1; i < riadok.length() - 1; i++) {
      groupName  += riadok.at(i);
    }
    if (!nastavSkupinu(groupName)) {
      pridajSkupinu(groupName);
    }
    return true;
  }
  else {
    string kluc, hodnota;
    bool prva = true;
    for (unsigned int i = 0; i < riadok.length() && riadok[i] != '\n'; i++) {
      if (prva == true) {
        if (riadok[i] == '=') {
          prva = false;
        }
        else {
          kluc += riadok[i];
        }
      }
      else {
        hodnota += riadok[i];
      }
    }
    if ((kluc.length() == 0) || (hodnota.length() == 0))  {
      return false;
    }
    return pridajZaznam(kluc, hodnota);
  }
}
string zrusRiadiaceZnaky(string riadok) {
  string temp = "";
  for (unsigned int i = 0; i < riadok.length(); i++) {
    switch(riadok[i]) {
      case '\n':
      case '\t':
      case '\r':
      case '\b':
      case '\v':
      case '\0':
         break;
      default:
         temp += riadok[i];
         break;
    }
  }
  return temp;
}
//nacita konfiguraciu zo suboru
bool SKUPINY::nacitaj(string cesta) {
	multiLineComment = false;
  ifstream vstup(cesta.c_str());
  if(!vstup) {
    return false;
  }
  string riadok;
  stringstream odstraneneKomenty;

  while(getline(vstup, riadok)) {
    riadok = odstranKomenty(riadok);
    odstraneneKomenty << riadok;
    if (!multiLineComment){
    	odstraneneKomenty << endl;
    }
  }

  while(getline(odstraneneKomenty, riadok)) {
  	riadok = zrusRiadiaceZnaky(riadok);
  	spracujRiadok(riadok);
  }

  vstup.close();
  nastavSkupinu("");
  return true;
}

string SKUPINY::odstranDvojlomitka(string riadok){
	unsigned int pos = riadok.find("//");
	if (pos != string::npos){
		return riadok.substr(0,pos);
	}
	return riadok;
}

string SKUPINY::odstranKomenty(string riadok){
	if (!multiLineComment){
		unsigned int pos = riadok.find("/*");
		if (pos != string::npos){
			multiLineComment = true;
			return riadok.substr(0,pos);
		}
		else {
			return riadok;
		}
	}
	else {
		unsigned int pos = riadok.find("*/");
		if (pos != string::npos){
			multiLineComment = false;
			return riadok.substr(pos + 2,riadok.length());
		}
		else {
			return "";
		}
	}

}


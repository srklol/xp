#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>

using namespace std;

#include "eval.h"
#include "LegacyCode.h"
#include "testy.h"

int main(){
   bool result = true;

   result &= testyExistujucehoKodu();
   result &= testyKlucov();
   result &= testyDvojlomitka();
   result &= testyKomentaru();

  if(result) cout << endl << "All tests - OK" << endl ;
  else       cout << endl << "All tests - FAILED" << endl ;

}


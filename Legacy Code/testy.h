void vypisVysledkuTestu(bool result, string menoTestu){
   cout << "------------> " << menoTestu << ":  - ";
   cout << (result ? "OK" : "FALSE") << endl << endl;
}

bool testyExistujucehoKodu(){
  bool result = true;

  SKUPINA testSkup("");
  result &= testEqual(testSkup.nazov(), "", "nazov skupiny");
  result &= testEqual(testSkup.pocetZaznamov(), 0, "pocet 0");
  result &= testEqual(testSkup.vratAktualnyKluc(), "Prazdny", "vrat act kluc ip");
  result &= testEqual(testSkup.vratAktualnuHodnotu(), "Prazdny", "vrat act hodnotu ip");
  result &= testFalse(testSkup.pridajZaznam("",""), "Prazdny kluc insert");
  result &= testEqual(testSkup.vratAktualnyKluc(), "Prazdny", "vrat act kluc ip");
  result &= testEqual(testSkup.vratAktualnuHodnotu(), "Prazdny", "vrat act hodnotu ip");

  result &= testTrue(testSkup.pridajZaznam("IP","192.168.0.1"), "insert ip");
  result &= testEqual(testSkup.vratAktualnyKluc(), "IP", "vrat act kluc ip");
  result &= testEqual(testSkup.vratAktualnuHodnotu(), "192.168.0.1", "vrat act hodnotu ip");

  result &= testTrue(testSkup.pridajZaznam("Nazov","PC1234"), "insert nazov");
  result &= testTrue(testSkup.pridajZaznam("Administrator","Andrej"), "insert admin");
  result &= testEqual(testSkup.vratAktualnyKluc(), "Administrator", "vrat act kluc ip");
  result &= testEqual(testSkup.vratAktualnuHodnotu(), "Andrej", "vrat act hodnotu ip");
  result &= testEqual(testSkup.pocetZaznamov(), 3, "pocet 3");

  result &= testFalse(testSkup.pridajZaznam("IP","192.168.0.1"), "insert ip");
  result &= testEqual(testSkup.vratAktualnyKluc(), "IP", "vrat act kluc ip po false inserte");
  result &= testEqual(testSkup.vratAktualnuHodnotu(), "192.168.0.1", "vrat act hodnotu ip po false inserte");
  result &= testEqual(testSkup.pocetZaznamov(), 3, "pocet 3");

  result &= testFalse(testSkup.zmazZaznam("blablabla"), "zmaz neexistujuci");
  result &= testTrue(testSkup.zmazZaznam("IP"), "zmaz prvy");
  result &= testEqual(testSkup.pocetZaznamov(), 2, "pocet 2");
  result &= testTrue(testSkup.zmazZaznam("Administrator"), "zmaz posledny");
  result &= testEqual(testSkup.vratAktualnyKluc(), "Nazov", "vrat act kluc nazov");
  result &= testEqual(testSkup.vratAktualnuHodnotu(), "PC1234", "vrat act hodnotu nazov");

  SKUPINY s;
  result &= testFalse(s.nacitaj("configbla.txt"), "nacitanie fake configu");
  result &= testEqual(s.pocetSkupin(), 1, "pocet pred nacitanim");
  result &= testEqual(s.nazovAktualnejSkupiny(), "", "prva prazdna skupina");
  result &= testTrue(s.pridajSkupinu("testSkupina"), "test skupina");
  result &= testTrue(s.pridajZaznam("key1", "val1"), "zaznam do test skupiny");
  result &= testTrue(s.pridajZaznam("key2", "val2"), "zaznam do test skupiny");
  result &= testEqual(s.vratHodnotu("key1"), "val1", "test skupina -> key1");
  result &= testEqual(s.vratHodnotu("key2"), "val2", "test skupina -> key2");
  result &= testEqual(s.pocetZaznamov(), 2, "test skupina pocet zaznamov");
  result &= testTrue(s.nastavSkupinu(""), "nastav empty skupinu");
  result &= testEqual(s.pocetZaznamov(), 0, "empty skupina pocet zaznamov");

  SKUPINY ss1;
  result &= testTrue(ss1.nacitaj("config.txt"), "nacitanie config.txt");
  result &= testEqual(ss1.pocetSkupin(), 3, "pocet po nacitani");
  result &= testEqual(ss1.nazovAktualnejSkupiny(), "", "skupina po nacitani");
  result &= testEqual(ss1.vratHodnotu("IP"), "196.168.0.1", "test empty skupina -> IP");
  result &= testFalse(ss1.nastavSkupinu("bla"), "nastav zlu skupinu");
  result &= testTrue(ss1.nastavSkupinu("Test"), "nastav test skupinu");
  result &= testEqual(ss1.pocetZaznamov(), 3, "test skupina pocet zaznamov");
  result &= testEqual(ss1.vratAktualnuHodnotu(), "IT centrum admin", "test skupina aktualny");

  result &= testFalse(ss1.nastavZaznam("bla"), "nastav zly zaznam");
  result &= testTrue(ss1.nastavZaznam("IP"), "nastav IP zaznam");
  result &= testTrue(ss1.vlozAktualnuHodnotu("196.168.0.1"), "nastav test ip");
  result &= testEqual(ss1.vratAktualnuHodnotu(), "196.168.0.1", "test skupina aktualny");


  vypisVysledkuTestu(result, "Testy existujuceho kodu");
  return result;
}

bool testyKlucov(){
  bool result = true;

  SKUPINA testSkup("prva");
  result &= testFalse(testSkup.pridajZaznam("...;;'][,.", "nepridane"), "iba zle znaky v kluci nepridane");
  result &= testTrue(testSkup.pridajZaznam("Admi.nistra.tor ", "Andrej"), "iba zle znaky v kluci nepridane");
  result &= testEqual(testSkup.vratAktualnuHodnotu(), "Andrej", "vrat act hodnotu ip");

  SKUPINY ss1;
  result &= testTrue(ss1.nacitaj("configNew.txt"), "nacitanie config.txt");
  result &= testFalse(ss1.nastavZaznam("Admi.nistra.tor "), "nastav Admi.nistra.tor  zaznam");
  result &= testTrue(ss1.nastavZaznam("Administrator"), "nastav Administrator  zaznam");


  vypisVysledkuTestu(result, "Testy zaznamov");
  return result;
}

bool testyDvojlomitka(){
  bool result = true;

  SKUPINY ss1;
  result &= testFalse(ss1.spracujRiadok("[dobry //format]"), "zla skupina");
  result &= testTrue(ss1.spracujRiadok("IP=196.168//.0.1"), "dvojlomitko v hodnote");
  result &= testEqual(ss1.nazovAktualnejSkupiny(), "", "skupina po nacitani");
  result &= testEqual(ss1.vratAktualnuHodnotu(), "196.168", "test skupina aktualny");

  vypisVysledkuTestu(result, "Testy zaznamov");
  return result;
}

bool testyKomentaru(){
   bool result = true;

   SKUPINY ss1;
   result &= testTrue(ss1.nacitaj("configNew.txt"), "nacitanie config.txt");
   result &= testFalse(ss1.nastavSkupinu("Test"), "nastav test skupinu");
   result &= testTrue(ss1.nastavSkupinu("TestProdukcia"), "nastav testprodukcia skupinu");
   result &= testEqual(ss1.nazovAktualnejSkupiny(), "TestProdukcia", "skupina po nacitani");
   result &= testFalse(ss1.dalsi(), "dalsi");


  vypisVysledkuTestu(result, "Testy nacitania zo suboru");
  return result;
}

#include <cstring>
#include <inttypes.h>

/*
typedef signed __int64       int64_t;
typedef unsigned __int64     uint64_t;
*/

const bool DUMMY_BOOL = false;
const int DUMMY_INT = 0;
const string DUMMY_STRING = "II";
const int64_t MAX_NUMBER = 3999999999;

const char operands[21] = {'O', 'I', 'V', 'X', 'L', 'C', 'D', 'M', 'P', 'Q', 'R', 'S', 'T', 'U', 'W', 'Y', 'Z', 'E', 'F', 'G', '\0'};
//const char samostat[4] = {'V', 'L', 'D', '\0'};
const char odpoc[10] = {'I', 'X', 'C', 'M', 'Q', 'S', 'U', 'Y', 'E', '\0'};
const char oper[10] = {'+', '-', '*', '/', '&', '|', '>', '<', '=', '\0'};
const int64_t hodnoty[20] = {0, 1, 5, 10, 50, 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000, 10000000, 50000000, 100000000, 500000000, 1000000000};

const string CISLO_MIMO = "Cislo mimo povolenych hodnot";
const string ZLY_VYRAZ = "Zly vstupny vyraz";
const char MAX_CHAR = operands[strlen(operands) - 1];

class RIMSKA_KALKULACKA {
	string prvy, druhy;
	int opIndex;
	//char operands[7];
	//char oper[4];
	char op;
	//int hodnoty[7];

 public:
  RIMSKA_KALKULACKA();
  string kalkulackaRimska(const string vyraz);
  bool hasOperator(string vyr);
  bool isInArray(const char* pole,char z);
  int getCharIndex(const char* pole,char z);
  bool hasOperands(string vyr);
  bool isCorrect(string _operand);
  int getOpIndex(string vyr);
  string convertToRim(int64_t cislo);
  int64_t convertToArab(string operand);
  int64_t spocitaj(int64_t first, int64_t second);
  int firstSign, secondSign;
};

RIMSKA_KALKULACKA::RIMSKA_KALKULACKA() : opIndex(1){
	firstSign = 1;
	secondSign = 1;
}

string RIMSKA_KALKULACKA::kalkulackaRimska(string vyr) {

	//return ZLY_VYRAZ;
	if (vyr == ""){
		return ZLY_VYRAZ;
	}
	if (!hasOperator(vyr)){
		return ZLY_VYRAZ;
	}

	if (!hasOperands(vyr)){
		return ZLY_VYRAZ;
	}

	if (!isCorrect(prvy) || !isCorrect(druhy)){
		return ZLY_VYRAZ;
	}
	//cout << convertToArab(prvy) << endl;

	int64_t num1 = convertToArab(prvy)*firstSign;
	int64_t num2 = convertToArab(druhy)*secondSign;
	int64_t intResult = 0;

	//int64_t intResult = spocitaj(num1,num2);
	//return  convertToRim(spocitaj(num1,num1));
	//int64_t intResult = spocitaj(convertToArab(prvy)*firstSign,convertToArab(druhy)*secondSign);
	if (num2 == 0 && op == '/'){
		return ZLY_VYRAZ;
	}

	/*z nejakeho dovodu ked sme mali spocitanie switchom alebo v metode spocitaj nam to hadzalo ze mame memleak na serveri
	 * tak som to vytiahol sem
	 */

	if (op == '+'){
		intResult = num1 + num2;
	}
	if (op == '-'){
		intResult = num1 - num2;
	}
	if (op == '*'){
		intResult = num1 * num2;
	}
	if (op == '/'){
		intResult = num1 / num2;
	}
	if (op == '&'){
		intResult = (num1 > 0 && num2 > 0) ? 1 : 0;
	}
	if (op == '&'){
		intResult = (num1 > 0 && num2 > 0) ? 1 : 0;
	}
	if (op == '&'){
		intResult = (num1 > num2) ? 1 : 0;
	}
	if (op == '&'){
		intResult = (num1 < num2) ? 1 : 0;
	}
	if (op == '&'){
		intResult = (num1 == num2) ? 1 : 0;
	}

	if (intResult > MAX_NUMBER){
		return CISLO_MIMO;
	}

	return convertToRim(intResult);
}

bool RIMSKA_KALKULACKA::hasOperator(const string vyr){
	bool result = false;
	bool haveFirst = false, haveSecond = false;
	bool haveOp = false;
	char _op;
	firstSign = 1;
	secondSign = 1;
	for (unsigned int i = 0; i < vyr.length(); i++){
		if (vyr[i] == '-'){
			if (!haveFirst){
				firstSign = -1;
			}
			else {
				if (haveOp){
					secondSign = -1;
				}
				else {
					_op = vyr[i];
					opIndex = i;
					haveOp = true;
				}
			}
		}
		else {
			if (isInArray(oper, vyr[i])){
				if (!haveFirst){
					return false;
				}
				else {
					_op = vyr[i];
					opIndex = i;
					haveOp = true;
				}
			}
			else {
				if (isInArray(operands, vyr[i])){
					if (!haveOp){
						haveFirst = true;
					}
					else {
						haveSecond = true;
						result = true;
					}
				}
			}
		}
	}
/*
  bool haveFirst = false, haveSecond = false;
	bool result = false;
	bool haveOp = false;
	char _op;
	for (unsigned int i = 0; i < vyr.length(); i++){
		if (!haveFirst){
			if (isInArray(oper, vyr[i])){
				if (vyr[i] == '-'){
					firstSign = -1;
				}
			}
			else {
				if (isInArray(operands, vyr[i])){
					haveFirst = true;
				}
			}
		}
		else {
			if (!haveSecond){
				if (isInArray(oper, vyr[i])){
					if (!haveOp){
						_op = vyr[i];
						opIndex = i;
						result = true;
						haveOp = true;
					}
					else {
						if (vyr[i] == '-'){
							secondSign = -1;
						}
					}
				}
			}
		}
	}*/
	if (result){
		op = _op;

	}
	if (firstSign < 0 && secondSign < 0 && (op == '*' || op == '/')){
		firstSign = 1;
		secondSign = 1;
	}
	return result;
}

bool RIMSKA_KALKULACKA::isInArray(const char* pole,char z){
	for (unsigned int j = 0; j < strlen(pole); j++){
		if (pole[j] == z) {
			return true;
		}
	}
	return false;
}

int RIMSKA_KALKULACKA::getCharIndex(const char* pole, char z){
	for (unsigned int i = 0; i < strlen(pole); i++){
		if (pole[i] == z){
			return i;
		}
	}
	return -1;
}

bool RIMSKA_KALKULACKA::hasOperands(string vyr){
	bool result = false;
	string _prvy = "", _druhy = "";
	for (int i = 0; i < opIndex; i++){
		if (isInArray(operands, vyr[i])){
			_prvy += vyr[i];
			result = true;
		}
		else {
			if (vyr[i] != ' ' && !isInArray(oper, vyr[i])){
				return false;
			}
		}
	}
	if (_prvy == ""){
		return false;
	}
	for (unsigned int i = opIndex + 1; i < vyr.length(); i++){
		if (isInArray(operands, vyr[i])){
			_druhy += vyr[i];
			result = true;
		}
		else {
			if (vyr[i] != ' ' && !isInArray(oper, vyr[i])){
				return false;
			}
		}
	}
	if (_druhy == ""){
		return false;
	}
	if (result){
			prvy = _prvy;
			druhy = _druhy;
	}
	return result;
}

bool RIMSKA_KALKULACKA::isCorrect(string _operand){
	char tChar = _operand[0];    //tChar o jeden za cCharom, cChar mysleny ako current
	//getCharIndex(operands, _operand[0]);
	int sameCounter = 0;
	char prevOdpoc = '/';
	for (unsigned int i = 1; i < _operand.size(); i++){
		if (tChar == _operand[i]){
			sameCounter++;
			if (sameCounter >= 3){
				return false;
			}
			if (!isInArray(odpoc, tChar) && tChar != MAX_CHAR){
				return false;
			}
			if (i > 1){
				if (getCharIndex(operands, _operand[i - 2]) < getCharIndex(operands, tChar)){
					return false;
				}
			}
		}
		else {
			char cChar = _operand[i];
			if (sameCounter >= 1){
				if (getCharIndex(operands, cChar) > getCharIndex(operands, tChar)){
					//cout << "kalabas" << endl;
					return false;
				}
			}
			if (!isInArray(odpoc,tChar) && tChar != MAX_CHAR){
				/*
				 * ked tam nieje znak ktory zmensuje napr IX etc.
				 */
				if (sameCounter >= 1){
					return false;
				}
				if (getCharIndex(operands, cChar) > getCharIndex(operands, tChar)){
					return false;
				}
			}
			else {
				if (getCharIndex(operands, cChar) > getCharIndex(operands, tChar)){
					if (getCharIndex(operands, cChar) - getCharIndex(operands, tChar) > 2){
						return false;
					}
				}
			}

			//maximum ojeb
			if (i < _operand.length() - 1){
				if (_operand[i-1] == _operand[i + 1]){
					if (getCharIndex(operands, _operand[i - 1]) - getCharIndex(operands, cChar) != 2){
						return false;
					}
				}
				if (getCharIndex(operands, tChar) - getCharIndex(operands, cChar) == 1){
					if (getCharIndex(operands, _operand[i + 1]) - getCharIndex(operands, cChar) >= 2){
						return false;
					}
				}
			}

			sameCounter = 0;
		}
		tChar = _operand[i];
	}
	return true;
}

/*
int RIMSKA_KALKULACKA::getOpIndex(string vyr){
	for (unsigned int i = 0; i < vyr.length(); i++){
		if (isInArray(oper, vyr[i])){
			return i;
		}
	}
	return -1;
}
*/

string RIMSKA_KALKULACKA::convertToRim(int64_t cislo){
	//int cislo = pcislo;

	string result = "";

	if (cislo == 0){
		result += operands[0];
	}
	else{
		if (cislo < 0){
			result += "-";
			cislo = -cislo;
		}
		int operandIndex = 19;
		int nas = 0;
		while (cislo > 0){
			for (int i = 0; i < cislo / hodnoty[operandIndex] ; i++){
				nas++;
			}
			if (nas > 0){
				if (nas > 3){
					if (result[result.size() - 1] != operands[operandIndex + 1]){
						//if (!isInArray(samostat, result[result.size() - 1])){
						result += operands[operandIndex];
						result += operands[operandIndex + 1];
					}
					else {
						result = result.substr(0, result.size()-1);
						result += operands[operandIndex];
						result += operands[operandIndex + 2];
					}
				}
				else {
					for (int i = 0; i < nas; i++){
						result += operands[operandIndex];
					}
				}
				//cout << result << endl;
				cislo -= nas*hodnoty[operandIndex];
				nas = 0;
			}
			operandIndex--;
		}
	}
	return result;
}

int64_t RIMSKA_KALKULACKA::convertToArab(string _operand){
	int64_t result = 0;
	char pChar = _operand[0];
	char cChar;
	bool naKonci = false;
	for (unsigned int i = 1; i < _operand.length(); i++){
		cChar = _operand[i];
		if (getCharIndex(operands, cChar) > getCharIndex(operands, pChar)){
			result += (hodnoty[getCharIndex(operands, cChar)] - hodnoty[getCharIndex(operands, pChar)]);
			i++;
			if (i == _operand.length()){
				naKonci = true;
			}
		}
		else {
			result += hodnoty[getCharIndex(operands, pChar)];
		}
		pChar = _operand[i];
	}
	if (!naKonci){
		result += hodnoty[getCharIndex(operands, _operand[_operand.length()-1])];
	}
	return result;
}

int64_t RIMSKA_KALKULACKA::spocitaj(int64_t first,int64_t second){
	int64_t result = 0;

	if (op == '+'){
		result = first + second;
	}
	if (op == '-'){
		result = first - second;
	}
	if (op == '*'){
		result = first * second;
	}
	if (op == '/'){
		result = first / second;
	}
	if (op == '&'){
		result = (first > 0 && second > 0) ? 1 : 0;
	}
	if (op == '&'){
		result = (first > 0 && second > 0) ? 1 : 0;
	}
	if (op == '&'){
		result = (first > second) ? 1 : 0;
	}
	if (op == '&'){
		result = (first < second) ? 1 : 0;
	}
	if (op == '&'){
		result = (first == second) ? 1 : 0;
	}
	/*
	switch (op){
		case '+':
			result = first + second;
			break;
		case '-':
			result = first - second;
			break;
		case '*':
			result = first * second;
			break;
		case '/':
			result = first / second;
			break;
		case '&':
			result = (first > 0 && second > 0) ? 1 : 0;
			break;
		case '|':
			result = (first > 0 || second > 0) ? 1 : 0;
			break;
		case '>':
			result = (first > second) ? 1 : 0;
			break;
		case '<':
			result = (first < second) ? 1 : 0;
			break;
		case '=':
			result = (first == second) ? 1 : 0;
			break;

	}
*/
	return result;
}

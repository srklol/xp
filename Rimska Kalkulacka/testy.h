void vypisVysledkuTestu(bool result, string menoTestu){
   cout << "------------> " << menoTestu << ":  - ";
   cout << (result ? "OK" : "FALSE") << endl << endl;
}

RIMSKA_KALKULACKA kalkulacka;
//#define ZNAK(str) hodnoty[kalkulacka.getCharIndex(operands, str)]
#define RIM(i) kalkulacka.convertToRim(i)
#define ZNAKNUM(nasobok, cislo) kalkulacka.znakHodnoty(nasobok, cislo)
#define CALC(vyraz) kalkulacka.kalkulackaRimska(vyraz)

bool testyRimskaKalkulacka(){
  bool result = true;


  result &= testEqual("II", kalkulacka.kalkulackaRimska("I + I"),"vypocet");
  result &= testEqual("IX", kalkulacka.kalkulackaRimska("X - I"),"X - I");
  result &= testEqual("O", kalkulacka.kalkulackaRimska("I +- I"),"I +- I dve znamienka za sebou");
  result &= testEqual("EG", kalkulacka.kalkulackaRimska("-QQQ * - QQQ"),"-QQQ * - QQQ");
  result &= testEqual("O", kalkulacka.kalkulackaRimska("-QQQ & - QQQ"),"-QQQ & - QQQ");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(""), "prazdny string");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska("MD"), "ziaden operator");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska("+"), "iba operator");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska("++"), "2 operator");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska("+*/"), "3 operator");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska("+  *    /"), "3 operator + medzery");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska("  +  "), "iba operator s medzerami vpredu aj vzadu");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska("  + X "), "medzery operator cislo");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" X + "), " cislo operator medzery");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" XAX + "), " cislo zly znak operator medzery");
  result &= testEqual("XX",kalkulacka.kalkulackaRimska(" X + X "), " cislo operator cislo");
  result &= testEqual("L",kalkulacka.kalkulackaRimska(" XL + X "), " cislo operator cislo");
  result &= testEqual("MMXCI",kalkulacka.kalkulackaRimska(" MMXC + I"), " MMXC");
  result &= testEqual("L",kalkulacka.kalkulackaRimska(" XLIX + I"), " XLIX");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" IICM + I"), " zle poradie znakov IICM");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" I*I+C/M + I"), " zle poradie znakov viac operatorov IICM");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MDM + I"), " zle poradie znakov");
  result &= testEqual("MMCDI",kalkulacka.kalkulackaRimska(" MMCD + I"), " MMCD");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MMMMDD + I"), " cislo zly znak operator medzery");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" IIII + I"), " IIII + I CISLO MIMO");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MMMDDDIII + I"), " MMMDDDIII");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MMMDDDIII + I"), " MMMDDDIII");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MMMDDDIIII + I"), " MMMDDDIIII ZLY_VYRAZ");
  result &= testEqual("MMMCDXCI",kalkulacka.kalkulackaRimska(" MMMCDXC + I"), " MMMCDXC");
  result &= testEqual("MMM",kalkulacka.kalkulackaRimska(" MD + MD"), " MD + MD");
  result &= testEqual("IV", kalkulacka.kalkulackaRimska("II + II"),"vypocet");
  result &= testEqual("XL", kalkulacka.kalkulackaRimska("XX + XX"),"vypocet");
  result &= testEqual("XLII", kalkulacka.kalkulackaRimska("XXII + XX"),"vypocet");
  result &= testEqual("MCCXXII", kalkulacka.kalkulackaRimska("MMCDXLIV - MCCXXII"),"MMCDXLIV - MCCXXII");
  result &= testEqual("O", kalkulacka.kalkulackaRimska("XXII / XXX"),"vypocet");
  result &= testEqual("I", kalkulacka.kalkulackaRimska("XXX III / XXX"),"vypocet");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("IVI - I"),"vypocet");
  result &= testEqual("C", kalkulacka.kalkulackaRimska("XCIX + I"),"vypocet");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" IXIXIXIXIXIX + I"), " IXIXIXIXIXIX ZLY_VYRAZ");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MCDXIC + I"), " MCDXIC ZLY_VYRAZ");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MCMM + I"), " MCMM ZLY_VYRAZ");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MCMCMC + I"), " MCMCMC ZLY_VYRAZ");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" DM + I"), " DM ZLY_VYRAZ");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("IVXI - I")," IVXI ZLY VYRAZ");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("VIIX - I"),"VIIX ZLY VYRAZ");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("VIX - I"),"VIX ZLY VYRAZ");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("VIV - I"),"VIV ZLY VYRAZ");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("DCM - I"),"DCM ZLY VYRAZ");
  result &= testEqual("LXIII", kalkulacka.kalkulackaRimska("LXIV - I"),"LXIV vypocet");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("VX - I"),"VX ZLY VYRAZ");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("DCCMX - I"),"DCCMX ZLY VYRAZ");
  result &= testEqual("XIII", kalkulacka.kalkulackaRimska("XIV - I"),"XIV vypocet");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska("XIIV - I"),"XIIV ZLY VYRAZ");
  result &= testEqual("MMMCMXCVII", kalkulacka.kalkulackaRimska(" M M MC MX CIX - I I "), " M M MC MX CIX - I I ");
  result &= testEqual("MCMI", kalkulacka.kalkulackaRimska(" MCM + I "), " MCM + I ");
  result &= testEqual("CXCI", kalkulacka.kalkulackaRimska(" CXC + I "), " CXC + I ");
  result &= testEqual("XX", kalkulacka.kalkulackaRimska(" XIX + I "), " XIX + I ");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska(" VV + I "), " VV + I ");
  result &= testEqual("XXI", kalkulacka.kalkulackaRimska(" XX + I "), " XX + I ");
  result &= testEqual(ZLY_VYRAZ, kalkulacka.kalkulackaRimska(" DD + I "), " DD + I ");
  result &= testEqual("DCI", kalkulacka.kalkulackaRimska(" DC + I "), " DC + I ");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MCMMMCMM + I"), " MCMMMCMM ZLY_VYRAZ");
  result &= testEqual(ZLY_VYRAZ,kalkulacka.kalkulackaRimska(" MCMMCMM + I"), " MCMMCMM ZLY_VYRAZ");
  result &= testEqual("-II",kalkulacka.kalkulackaRimska("GGEGYSTQQQVII - GGEGYSTQQQIX"), "GGEGYSTQQQVII - GGEGYSTQQQIX");


  vypisVysledkuTestu(result, "Kalkulacka s rimskymi cislami");
  return result;
}

